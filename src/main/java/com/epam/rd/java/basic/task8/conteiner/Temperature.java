package com.epam.rd.java.basic.task8.conteiner;

public class Temperature {
    private String measure;
    private int temp;

    public Temperature() {
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    @Override
    public String toString() {
        return "Temperature{" +
                "measure='" + measure + '\'' +
                ", temp=" + temp +
                '}';
    }
}
