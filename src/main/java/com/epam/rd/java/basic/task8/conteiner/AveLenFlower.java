package com.epam.rd.java.basic.task8.conteiner;

public class AveLenFlower {
    private String measure;
    private int length;

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "AveLenFlower{" +
                "measure='" + measure + '\'' +
                ", length=" + length +
                '}';
    }
}
