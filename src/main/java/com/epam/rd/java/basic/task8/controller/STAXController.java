package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.conteiner.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> initialize() {
        List<Flower> flowers = new ArrayList<>();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        try {
            XMLEventReader eventReader = factory.createXMLEventReader(new FileInputStream(xmlFileName));
            Flower flower = null;
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case "flower":
                            flower = new Flower();
                            VisualParameters visualParameters = new VisualParameters();
                            flower.setVisualParameters(visualParameters);
                            AveLenFlower aveLenFlower = new AveLenFlower();
                            visualParameters.setAveLenFlower(aveLenFlower);
                            GrowingTips growingTips = new GrowingTips();
                            flower.setGrowingTips(growingTips);
                            Temperature temperature = new Temperature();
                            growingTips.setTemperature(temperature);
                            Watering watering = new Watering();
                            growingTips.setWatering(watering);
                            break;
                        case "name": {
                            flower.setName(eventReader.nextEvent().asCharacters().getData());
                            break;
                        }
                        case "soil": {
                            flower.setSoil(eventReader.nextEvent().asCharacters().getData());
                            break;
                        }
                        case "origin": {
                            flower.setOrigin(eventReader.nextEvent().asCharacters().getData());
                            break;
                        }
                        case "stemColour": {
                            visualParameters = flower.getVisualParameters();
                            visualParameters.setStemColour(eventReader.nextEvent().asCharacters().getData());
                            break;
                        }
                        case "leafColour": {
                            visualParameters = flower.getVisualParameters();
                            visualParameters.setLeafColour(eventReader.nextEvent().asCharacters().getData());
                            break;
                        }
                        case "aveLenFlower": {
                            visualParameters = flower.getVisualParameters();
                            aveLenFlower = visualParameters.getAveLenFlower();
                            aveLenFlower.setMeasure(startElement.getAttributeByName(new QName("measure")).getValue());
                            aveLenFlower.setLength(Integer.parseInt(eventReader.nextEvent().asCharacters().getData()));
                            break;
                        }
                        case "tempreture": {
                            growingTips = flower.getGrowingTips();
                            temperature = growingTips.getTemperature();
                            temperature.setMeasure(startElement.getAttributeByName(new QName("measure")).getValue());
                            temperature.setTemp(Integer.parseInt(eventReader.nextEvent().asCharacters().getData()));
                            break;
                        }
                        case "lighting": {
                            growingTips = flower.getGrowingTips();
                            growingTips.setLighting(startElement.getAttributeByName(new QName("lightRequiring")).getValue());
                            break;
                        }
                        case "watering": {
                            growingTips = flower.getGrowingTips();
                            watering = growingTips.getWatering();
                            watering.setMeasure(startElement.getAttributeByName(new QName("measure")).getValue());
                            watering.setValue(Integer.parseInt(eventReader.nextEvent().asCharacters().getData()));
                            break;
                        }
                        case "multiplying": {
                            flower.setMultiplying(Multiplying.fromName(eventReader.nextEvent().asCharacters().getData()));
                            break;
                        }
                    }
                }
                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("flower")) {
                        flowers.add(flower);
                    }
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return flowers;
    }


}