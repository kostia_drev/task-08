package com.epam.rd.java.basic.task8.writer;

import com.epam.rd.java.basic.task8.conteiner.Flower;
import com.epam.rd.java.basic.task8.conteiner.GrowingTips;
import com.epam.rd.java.basic.task8.conteiner.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.List;

public class DOMWriter {

    private String xmlFileName;

    public DOMWriter(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void setXmlFileName(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void writeInXML(List<Flower> flowers) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("flowers");
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
        doc.appendChild(rootElement);
        flowers.forEach(flower -> {
            Element element = doc.createElement("flower");
            rootElement.appendChild(element);
            element.appendChild(createElement(doc, "name", flower.getName()));
            element.appendChild(createElement(doc, "soil", flower.getSoil()));
            element.appendChild(createElement(doc, "origin", flower.getOrigin()));
            Element visualParametersElement = createElement(doc, "visualParameters", "");
            VisualParameters visualParameters = flower.getVisualParameters();
            visualParametersElement.appendChild(createElement(doc, "stemColour", visualParameters.getStemColour()));
            visualParametersElement.appendChild(createElement(doc, "leafColour", visualParameters.getLeafColour()));
            visualParametersElement.appendChild(createElementWithAttribute(doc, "aveLenFlower",
                    String.valueOf(visualParameters.getAveLenFlower().getLength()),
                    "measure", visualParameters.getAveLenFlower().getMeasure()));
            element.appendChild(visualParametersElement);
            Element growingTipsElement = createElement(doc, "growingTips", "");
            GrowingTips growingTips = flower.getGrowingTips();
            growingTipsElement.appendChild(createElementWithAttribute(doc, "tempreture",
                    String.valueOf(growingTips.getTemperature().getTemp()),
                    "measure", growingTips.getTemperature().getMeasure()));
            growingTipsElement.appendChild(createElementWithAttribute(doc, "lighting", "",
                    "lightRequiring", growingTips.getLighting()));
            growingTipsElement.appendChild(createElementWithAttribute(doc, "watering",
                    String.valueOf(growingTips.getWatering().getValue()),
                    "measure", growingTips.getWatering().getMeasure()));
            element.appendChild(growingTipsElement);
            element.appendChild(createElement(doc, "multiplying", flower.getMultiplying().getName()));
        });
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(xmlFileName);

        transformer.transform(source, result);
    }

    private Element createElementWithAttribute(Document doc, String name, String text, String attributeName, String attributeValue) {
        Element element = createElement(doc, name, text);
        element.setAttribute(attributeName, attributeValue);
        return element;
    }

    private Element createElement(Document doc, String name, String text) {
        Element element = doc.createElement(name);
        element.setTextContent(text);
        return element;
    }
}
