package com.epam.rd.java.basic.task8.conteiner;

public class GrowingTips {
    private Temperature temperature;
    private String lighting;
    private Watering watering;

    public GrowingTips() {
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", lighting='" + lighting + '\'' +
                ", watering=" + watering +
                '}';
    }
}
