package com.epam.rd.java.basic.task8.conteiner;

public enum Multiplying {
    LEAVES("листья"), CUTTINGS("черенки"), SEEDS("семена"), NO_FOUND("не найдено");

    private final String name;

    Multiplying(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Multiplying fromName(String value) {
        for (final Multiplying multiplying : values()) {
            if (multiplying.name.equalsIgnoreCase(value)) {
                return multiplying;
            }
        }
        return NO_FOUND;
    }
}
