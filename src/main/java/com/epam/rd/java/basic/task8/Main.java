package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.conteiner.Flower;
import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import com.epam.rd.java.basic.task8.writer.DOMWriter;

import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        // PLACE YOUR CODE HERE

        List<Flower> DOMFlowers = domController.initialize();
        DOMFlowers.forEach(System.out::println);
        // sort (case 1)
        // PLACE YOUR CODE HERE

        // save
        String outputXmlFile = "output.dom.xml";
        DOMWriter writer = new DOMWriter(outputXmlFile);
        writer.writeInXML(DOMFlowers);
        // PLACE YOUR CODE HERE

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////
        System.out.println("\n-----------------------------------\n");
        // get
        SAXController saxController = new SAXController(xmlFileName);
        List<Flower> SAXFlowers = saxController.initialize();
        SAXFlowers.forEach(System.out::println);
        // PLACE YOUR CODE HERE

        // sort  (case 2)
        // PLACE YOUR CODE HERE

        // save
        outputXmlFile = "output.sax.xml";
        writer.setXmlFileName(outputXmlFile);
        writer.writeInXML(SAXFlowers);
        // PLACE YOUR CODE HERE

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////
        System.out.println("\n-----------------------------------\n");
        // get
        STAXController staxController = new STAXController(xmlFileName);
        List<Flower> STAXFlowers = staxController.initialize();
        STAXFlowers.forEach(System.out::println);
        // PLACE YOUR CODE HERE

        // sort  (case 3)
        // PLACE YOUR CODE HERE

        // save
        outputXmlFile = "output.stax.xml";
        writer.setXmlFileName(outputXmlFile);
        writer.writeInXML(STAXFlowers);
        // PLACE YOUR CODE HERE
    }
}
