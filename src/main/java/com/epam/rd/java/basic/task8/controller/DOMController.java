package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.conteiner.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {
    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    private Document getDocument() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new File(xmlFileName));
    }

    public List<Flower> initialize() {
        Document document = null;
        try {
            document = getDocument();
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        List<Flower> flowers = new ArrayList<>();
        if (document != null) {
            document.getDocumentElement().normalize();
            NodeList flowersList = document.getElementsByTagName("flower");
            for (int i = 0; i < flowersList.getLength(); ++i) {
                Node flowerNode = flowersList.item(i);
                if (flowerNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element flowerElement = (Element) flowerNode;
                    Flower flower = new Flower();

                    //introducing simple parameters
                    flower.setName(getElementValue(flowerElement, "name"));
                    flower.setSoil(getElementValue(flowerElement, "soil"));
                    flower.setOrigin(getElementValue(flowerElement, "origin"));

                    //introducing visual parameters
                    VisualParameters visualParameters = new VisualParameters();
                    Element visualParametersElement = (Element) flowerElement.getElementsByTagName("visualParameters").item(0);
                    visualParameters.setStemColour(getElementValue(visualParametersElement, "stemColour"));
                    visualParameters.setLeafColour(getElementValue(visualParametersElement, "leafColour"));
                    AveLenFlower aveLenFlower = new AveLenFlower();
                    aveLenFlower.setMeasure(getElementAttributeValue(visualParametersElement, "aveLenFlower", "measure"));
                    aveLenFlower.setLength(Integer.parseInt(getElementValue(visualParametersElement, "aveLenFlower")));
                    visualParameters.setAveLenFlower(aveLenFlower);
                    flower.setVisualParameters(visualParameters);

                    //introducing growing tips
                    GrowingTips growingTips = new GrowingTips();
                    Element growingTipsElement = (Element) flowerElement.getElementsByTagName("growingTips").item(0);
                    Temperature temperature = new Temperature();
                    temperature.setTemp(Integer.parseInt(getElementValue(growingTipsElement, "tempreture")));
                    temperature.setMeasure(getElementAttributeValue(growingTipsElement, "tempreture", "measure"));
                    growingTips.setTemperature(temperature);
                    growingTips.setLighting(getElementAttributeValue(growingTipsElement, "lighting", "lightRequiring"));
                    Watering watering = new Watering();
                    watering.setMeasure(getElementAttributeValue(growingTipsElement, "watering", "measure"));
                    watering.setValue(Integer.parseInt(getElementValue(growingTipsElement, "watering")));
                    growingTips.setWatering(watering);
                    flower.setGrowingTips(growingTips);

                    flower.setMultiplying(Multiplying.fromName(getElementValue(flowerElement, "multiplying")));

                    flowers.add(flower);
                }
            }
        }
        return flowers;
    }

    private String getElementAttributeValue(Element element, String tag, String attributeName) {
        Node node = element.getElementsByTagName(tag).item(0);
        NamedNodeMap attributes = node.getAttributes();
        return attributes.getNamedItem(attributeName).getNodeValue();
    }

    private String getElementValue(Element element, String tag) {
        return element.getElementsByTagName(tag).item(0).getTextContent();
    }
}