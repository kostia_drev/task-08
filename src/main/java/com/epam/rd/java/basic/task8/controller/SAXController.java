package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.conteiner.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final String xmlFileName;
    private static List<Flower> flowers;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        flowers = new ArrayList<>();
    }

    public List<Flower> initialize() {
        flowers.clear();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser;
        try {
            parser = factory.newSAXParser();
            AdvancedXMLHandler handler = new AdvancedXMLHandler();
            parser.parse(new File(xmlFileName), handler);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return flowers;
    }

    private static class AdvancedXMLHandler extends DefaultHandler {
        private Flower flower;
        private VisualParameters visualParameters;
        private AveLenFlower aveLenFlower;
        private GrowingTips growingTips;
        private Temperature temperature;
        private Watering watering;
        private String elementName;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            switch (qName) {
                case "flower":
                    flower = new Flower();
                    visualParameters = new VisualParameters();
                    flower.setVisualParameters(visualParameters);
                    aveLenFlower = new AveLenFlower();
                    visualParameters.setAveLenFlower(aveLenFlower);
                    growingTips = new GrowingTips();
                    flower.setGrowingTips(growingTips);
                    temperature = new Temperature();
                    growingTips.setTemperature(temperature);
                    watering = new Watering();
                    growingTips.setWatering(watering);
                    break;
                case "aveLenFlower": {
                    aveLenFlower.setMeasure(attributes.getValue("measure"));
                    break;
                }
                case "watering": {
                    watering.setMeasure(attributes.getValue("measure"));
                    break;
                }
                case "lighting": {
                    growingTips.setLighting(attributes.getValue("lightRequiring"));
                    break;
                }
                case "tempreture":
                    temperature.setMeasure(attributes.getValue("measure"));
                    break;
            }
            elementName = qName;

        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String information = new String(ch, start, length);
            information = information.replace("\n", "").trim();
            if (!information.isEmpty()) {
                switch (elementName) {
                    case "name": {
                        flower.setName(information);
                        break;
                    }
                    case "soil": {
                        flower.setSoil(information);
                        break;
                    }
                    case "origin": {
                        flower.setOrigin(information);
                        break;
                    }
                    case "stemColour": {
                        visualParameters.setStemColour(information);
                        break;
                    }
                    case "leafColour": {
                        visualParameters.setLeafColour(information);
                        break;
                    }
                    case "aveLenFlower": {
                        aveLenFlower.setLength(Integer.parseInt(information));
                        break;
                    }
                    case "tempreture": {
                        temperature.setTemp(Integer.parseInt(information));
                        break;
                    }
                    case "watering": {
                        watering.setValue(Integer.parseInt(information));
                        break;
                    }
                    case "multiplying": {
                        flower.setMultiplying(Multiplying.fromName(information));
                        break;
                    }
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (qName.equals("flower")) {
                flowers.add(flower);
            }
        }
    }
}